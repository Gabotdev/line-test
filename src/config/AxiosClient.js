import axios from 'axios';

import { LOCAL_STORAGE_KEYS, URI_API, URL_API } from '../constants.js';

const URL_BASE = URL_API;

const readUrl = (url) => {
	return url.startsWith('http://') || url.startsWith('https://')
		? url
		: `${URL_BASE}${url}`;
};

// Authorization Token
// const getToken = () => {
// 	return {
// 		token: sessionStorage.getItem(LOCAL_STORAGE_KEYS.accessToken),
// 	};
// };

const HEADERS_DEFAULT = {
	Accept: 'application/json',
	'Content-Type': 'application/json',
	'Access-Control-Allow-Origin': '*',
	'User-Agent': 'Line / 2.00.000'
};

const get = ({ url = '', options = {}, headers = {} }) => {

	return axios.get(readUrl(url), {
		headers: {
			...HEADERS_DEFAULT,
			Authorization: `${LOCAL_STORAGE_KEYS}`,
			...headers,
		},
		...options,
	});
};

const post = ({ url = '', body = {}, headers = {}, options = {} }) => {
	// const { token } = getToken();

	const { headers: headers_, ...restOptions } = options;

	return axios.post(readUrl(url), body, {
		headers: {
			...HEADERS_DEFAULT,
			...headers_,
			Authorization: `${LOCAL_STORAGE_KEYS}`,
		},
		...restOptions,
	});
};

const put = ({ url = '', body = {}, headers = {}, options = {} }) => {
	// const { token } = getToken();

	return axios.put(readUrl(url), body, {
		headers: {
			...HEADERS_DEFAULT,
			...headers,
			Authorization: `${LOCAL_STORAGE_KEYS}`,
		},
		...options,
	});
};

const _delete = ({ url = '', body = {}, headers = {}, options = {} }) => {
	// const { token } = getToken();

	return axios.delete(readUrl(url), {
		headers: {
			...HEADERS_DEFAULT,
			...headers,
			Authorization: `${LOCAL_STORAGE_KEYS}`,
		},
		...options,
	});
};

export default {
	get,
	post,
	put,
	delete: _delete,
};
