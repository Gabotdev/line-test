import {Router} from 'express';

import { 
    anulacionMontoPayment, 
    anulacionPayment, 
    createPayment, 
    getVerificationCode, 
    reversoPayment, 
    verificarCompra
} from '../controllers/PaymentsController.js';

export const PaymentRouters = () => {
	const routerRoot = Router();
    routerRoot.post('/', createPayment);
    routerRoot.post('/anulacion/:id', anulacionPayment);
    routerRoot.post('/anulacion/:id/:monto', anulacionMontoPayment);
    routerRoot.post('/reverso/:id', reversoPayment);
    routerRoot.post('/verificar-compra', verificarCompra);

    routerRoot.get('/test-code/:id', getVerificationCode);


    return routerRoot;
};