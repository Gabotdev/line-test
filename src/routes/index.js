import { Router } from 'express';

import { PaymentRouters } from './PaymentRoutes.js';

export default (app) => {
	const router = Router();


	return app.use(
        router.use('/payments', PaymentRouters())
	);
};