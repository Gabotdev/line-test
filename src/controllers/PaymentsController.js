import AxiosClient from "../config/AxiosClient.js"

export const createPayment = async (req, res) => {
    const { 
        IdentificadorCliente,  
        CanalVenta, 
        TerminalSistema, 
        TerminalTipo,
        CodigoEmisor,
        ModoIngreso,
        Moneda,
        NumeroTarjeta,
        FechaExpiracion,
        CodigoSeguridad,
        TarjetaTipo,
        IdentificadorComprador = "",
        CompraVerificada = [],
        TipoDocumento,
        DocumentoTitular,
        NombreTitular,
        EmailTitular,
        Referencia,
        Detalle = []
    } = req.body;
    let detalles = [];

    if (Detalle.length >= 1) {
        // TODO: Agregar validaciones
        detalles = Detalle.map((item) => item);
        
    } else {
        return res.status(400).json({
            "msg": 'Missing details about commerce'
        })
    }



    let body = {
        IdentificadorCliente,  
        CanalVenta, 
        TerminalSistema, 
        TerminalTipo,
        CodigoEmisor,
        ModoIngreso,
        Moneda,
        NumeroTarjeta,
        FechaExpiracion,
        CodigoSeguridad,
        TarjetaTipo,
        IdentificadorComprador,
        CompraVerificada,
        TipoDocumento,
        DocumentoTitular,
        NombreTitular,
        EmailTitular,
        Referencia,
        Detalle: detalles
    }

    try {
        const {data} = await AxiosClient.post({url: 'api/v1/creditcard/autorizacion', body});
        console.log(data)
        if (data.CodigoError == -1) {
            return res.status(400).json(data);
        }
        return res.json(data);
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            msg: "Ocurrió un error en el servidor"
        });
    }
}

export const anulacionPayment = async (req, res) => {
    const { id } = req.params;
    try {
        const {data} = await AxiosClient.post({url: `api/v1/creditcard/anulacion/${id}`});
        if (data.CodigoError == -1) {
            return res.status(400).json(data);
        }
        console.log(data)
        return res.json(data);
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            msg: "Ocurrió un error en el servidor"
        });    
    }
}

export const anulacionMontoPayment = async (req, res) => {
    const { id, monto } = req.params;
    const numberRegex = /[a-zA-Z!@#\$%\^\&*\)\(+=._-]/g;
    if ( numberRegex.test(monto) ) {
        return res.status(400).json({
            msg: "Monto is not a number."
        })
    }
    try {
        const {data} = await AxiosClient.post({url: `api/v1/creditcard/anulacion/${id}/${monto}`});
        if (data.CodigoError == -1) {
            return res.status(400).json(data);
        }
        console.log(data)
        return res.json(data);
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            msg: "Ocurrió un error en el servidor"
        });    
    }
}

export const reversoPayment = async (req, res) => {
    const { id } = req.params;
    try {
        const {data} = await AxiosClient.post({url: `api/v1/creditcard/reverso/${id}`});
        if (data.CodigoError == -1) {
            return res.status(400).json(data);
        }
        console.log(data)
        return res.json(data);
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            msg: "Ocurrió un error en el servidor"
        });    
    }
}

export const verificarCompra = async (req, res) => {
    const regexTime = /(MINUTO|HORA|DIA)/g;
    const regexIP = /^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$/;
    const {
        Codigo,
        CompraVerificadaID,
        CantidadMaximaIntentos = 0,
        CantidadTiempoExtensionVencimiento = 0,
        UnidadTiempoExtensionVencimiento = "",
        IPAddress = ""
    } = req.body;

    let body = {
        Codigo, 
        CompraVerificadaID
    }
    if (CantidadMaximaIntentos > 0) {
        body.CantidadMaximaIntentos = CantidadMaximaIntentos;
    }
    if (regexTime.test(UnidadTiempoExtensionVencimiento)) {
        body.UnidadTiempoExtensionVencimiento = UnidadTiempoExtensionVencimiento;
    }
    if (CantidadTiempoExtensionVencimiento > 0) {
        body.CantidadTiempoExtensionVencimiento = CantidadTiempoExtensionVencimiento;
    }
    if (regexIP.test(IPAddress)) {
        body.IPAddress = IPAddress;
    }
    console.log(body)
    try {
        const {data} = await AxiosClient.post({url: `api/v2/compraverificada/verificar`, body});
        console.log(data)
        if (data?.Validado) {
            return res.json(data);
        }
        if (data?.CodigoError == -1 || !data?.Validado) {
            return res.status(400).json(data);
        }
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            msg: "Ocurrió un error en el servidor"
        });    
    }
}

export const getVerificationCode = async (req, res) => {
    const { id } = req.params;
    try {
        const {data} = await AxiosClient.get({url: `api/v1/admin/svalbard/get/${id}`,  headers: {
            'Authorization': 'ApiKey 1e9c5592-b05c-40bc-83df-f65f4bfedda1',
        }});
        console.log(data)
        if (data?.Message) {
            return res.status(400).json(data);
        }
        return res.json(data);
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            msg: "Ocurrió un error en el servidor"
        });    
    }
}