import express from 'express';
import cors from 'cors';

import routes from './src/routes/index.js';

const app = express();
app.use(express.json())
app.use(cors());

const PORT = process.env.PORT || 4000;

routes(app);

app.all('/status', (req, res) => {
	res.json({
		active: true,
		message: 'Active and running server!',
	});
});

app.all('*', (req, res) => {
	res.json({
		active: true,
		message: 'Active and running server!',
	});
});


app.listen(PORT, () => {
    console.log(`Server is listening on port: ${PORT}`)
})